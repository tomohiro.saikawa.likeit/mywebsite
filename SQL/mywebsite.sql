CREATE DATABASE TRPG_USE DEFAULT CHARACTER SET utf8;
USE TRPG_USE;
CREATE TABLE user_date (id int primary key AUTO_INCREMENT,
                        name varchar(30) NOT NULL,
                        password varchar(10) NOT NULL,
                        voice_chat_flg boolean NOT NULL
                         );
CREATE TABLE user_select_category(id int primary key AUTO_INCREMENT,
                        user_id int NOT NULL,
                        category_id int NOT NULL);
                        
CREATE TABLE schedule(id int primary key AUTO_INCREMENT,
                        date date NOT NULL,
                        user_id int NOT NULL);
CREATE TABLE category_detail(category_id int primary key,
                       name varchar(255) NOT NULL );
                       
CREATE TABLE category_detail_two(category_id int primary key AUTO_INCREMENT,
                       name varchar(255) NOT NULL );
                       
CREATE TABLE session_date (id int primary key AUTO_INCREMENT,
                        password varchar(30),
                        detailed_text varchar(255) NOT NULL,
                        recruitment_date date NOT NULL,
                        date_and_time date NOT NULL,
                        voice_chat_flg boolean NOT NULL,
                        category_id int NOT NULL,
                        user_id int NOT NULL
                         );
 CREATE TABLE user_join(id int primary key AUTO_INCREMENT,
                        session_id int  NOT NULL,
                        user_id int NOT NULL);
                         
INSERT INTO user_date (name,password,voice_chat_flg) VALUES('to','ru',true);
SELECT * FROM user_date WHERE id = 3;

INSERT INTO session_date(password,
                       detailed_text,
                       recruitment_date,
                       date_and_time,
                       voice_chat_flg,
                       category_id,
                       user_id) VALUES('saisyo','お試しです','2020/11/26 10:51:29','2020/11/26 10:51:29',true,1,1);
SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id;
select * from staff inner join dept on staff.deptid = dept.id;


SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id  WHERE category_id = 4;
SELECT * FROM user_date ;
SELECT * FROM session_date WHERE id = 3;

UPDATE user_date  SET name = 'sai',password = 'nan',voice_chat_flg = true  WHERE id = 3;


INSERT INTO category_detail_two(name) VALUES('キルデスビジネス');
INSERT INTO user_select_category(user_id,category_id) VALUES(4,4);
