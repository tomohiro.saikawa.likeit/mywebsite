<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">




</head>
<body>

	<nav>

		<div class="row col-2 loginInfo">
			<p align="center">ユーザ名さん</p>
			<a href="link1.html">ログアウト</a>
		</div>

	</nav>
	<h1 style="text-align: center;">セッション詳細</h1>

	<form method="post" action="Usersignup">
		<div class="col-sm-12">
			<div align="center">
				<p align="center">ユーザーID ${userdetail.id}</p>
                <p align="center">パスワード ${userdetail.password}</p>
               <c:if test="${userdetail.voicechat == true }">
                     <p>可</p>
                     </c:if>
                     <c:if test="${userdetail.voicechat == false }">
                     <p>不可</p>
                     </c:if>

                      <c:if test="${user.voicechat == true }">
                     <td>可</td>
                     </c:if>




				<p>
					募集期日 ${userdetail.recruitment_date}
				</p>
            <p>
					開催日時 ${userdetail.date_and_time}
				</p>
            <p>詳細<br>
                <textarea name="comment" cols="80" rows="35">${userdetail.text}</textarea>


            </p>

				<p>
					<br>
				</p>
				<a  class="btn btn-primary"href="sessionParticipation">参加</button>
			</div>
		</div>
		<div aligin="right">
			<a href="javascript:history.back()">[戻る]</a>
		</div>


	</form>
</body>
</html>