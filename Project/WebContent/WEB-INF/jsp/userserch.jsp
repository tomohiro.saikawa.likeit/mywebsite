<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="Materialize/CSS/oldtext.css" rel="stylesheet" type="text/css" />





</head>








</head>
<body>
<form method="post" action="userserch">
     <div class="col-sm-12">
            <div align="center">
            <h1>
			<p href="/">プレイヤー検索</p>
		</h1>


		<h3>
			<p href="/">検索項目</p>
		</h3>


                <p align="center">プレイヤー名 </p><input type="text" name="name" size="30" >
                <p>ボイスチャット可能</p>
                可
                <input type="radio" name="onoroff" value="1">
                不可
                <input type="radio" name="onoroff" value="0">




                <p><br></p>
                <input type="submit" value="検索" class="btn btn-primary">
            </div>
        </div>
    <div class="col-sm-10">
			<div align="center">
<div class="table-responsive">
             <table class="table oldtext">
               <thead>
                 <tr>
                   <th>プレイヤーID</th>
                   <th>プレイヤー名</th>
                   <th>ボイスチャット</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach  items="${userList}" var="user"  >
                   <tr>







                     <td>${user.id}</td>
                     <td>${user.name}</td>
                     <c:if test="${user.voicechat == true }">
                     <td>可</td>
                     </c:if>
                     <c:if test="${user.voicechat == false }">
                     <td>不可</td>
                     </c:if>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->

                     <td>
                          <div class="row">
                       <a class="button" href="playrdetail?id=${user.id}">詳細</a>



                         </div>
                     </td>

                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
        </div>
    </div>
    </body>
</html>