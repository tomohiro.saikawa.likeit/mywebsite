package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String username, String Password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user_date WHERE name = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, username);
			pStmt.setString(2, Password);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String password_date= rs.getString("password");


			return new User(id, name, password_date);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//データべースに情報追加する内容、コード
	public void usersignup(String name, String password, boolean onoroff ) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user_date (name,password,voice_chat_flg) VALUES(?,?,?);";



			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,name);
			pStmt.setString(2, password);
			pStmt.setBoolean(3, onoroff);

			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	//パスワード暗号化メソッド

	public void usersignup2(String rimitdate,int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO schedule(date,user_id) VALUES(?,?);";



			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,rimitdate);
			pStmt.setInt(2,id);

			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public void usersignup3(String ct,int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user_select_category(user_id,category_id) VALUES(?,?);";



			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,ct);
			pStmt.setInt(2,id);

			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String Cgr(String password) {

		String source = password;

		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}
	//ユーザー情報検索用

	public List<User> userSearch(String ctid) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";



			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {

				int id = rs.getInt("user_id");
				Date recruitmentdate = rs.getDate("recruitment_date");
				Date dateandtime = rs.getDate("date_and_time");
				int categoryid = rs.getInt("category_id");
				String mid = rs.getString("name");
				User user = new User(id,recruitmentdate,dateandtime,categoryid,mid);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
		}



public User Browsing(int searchId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user_date WHERE id = ?;";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, searchId);

		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		int IdData = rs.getInt("id");
		String nameData = rs.getString("name");
		String passwordDate = rs.getString("password");
		Boolean voicechatflg = rs.getBoolean("voice_chat_flg");

		return new User(IdData,nameData,passwordDate,voicechatflg);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
public User Browsing2(String searchId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM session_date WHERE id = ?;";

		// SELECTを実行し、結果表を取得

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, searchId);

		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加



		int id = rs.getInt("id");
		String password = rs.getString("password");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		Boolean voicechat = rs.getBoolean("voice_chat_flg");
		int categoryid = rs.getInt("category_id");
		int mid = rs.getInt("user_id");
		String text = rs.getString("detailed_text");







		return new User(id,password,recruitmentdate,dateandtime,voicechat,categoryid,mid,text);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
public User Browsing3(String searchId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user_date WHERE id = ?;";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, searchId);

		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		int IdData = rs.getInt("id");
		String nameData = rs.getString("name");
		String passwordDate = rs.getString("password");
		Boolean voicechatflg = rs.getBoolean("voice_chat_flg");

		return new User(IdData,nameData,passwordDate,voicechatflg);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}

public void sessionsignup(String password,String detailedtext,String rimitdate,String opendate,Boolean voicechat,String ct,String userid){
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "INSERT INTO session_date(password,\r\n" +
				"detailed_text,\r\n" +
				"recruitment_date,\r\n" +
				"date_and_time,\r\n" +
				"voice_chat_flg,\r\n" +
				"category_id,\r\n" +
				"user_id) VALUES(?,?,?,?,?,?,?);";



		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);

		pStmt.setString(1,password);
		pStmt.setString(2,detailedtext);
		pStmt.setString(3,rimitdate);
		pStmt.setString(4,opendate);
		pStmt.setBoolean(5,voicechat);
		pStmt.setString(6,ct);
		pStmt.setString(7,userid);


		int result = pStmt.executeUpdate();

		System.out.println(result);
		pStmt.close();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
public List<User> findAll() {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id;";

	// SELECTを実行し、結果表を取得
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(sql);

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {

        int sid = rs.getInt("id");
		int id = rs.getInt("user_id");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		int categoryid = rs.getInt("category_id");
		String mid = rs.getString("name");
		User user = new User(sid,id,recruitmentdate,dateandtime,categoryid,mid);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}
public List<User> sessionsearch(String categoryid2) {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id  WHERE category_id = ?;";

	// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
	pStmt.setString(1, categoryid2);

	ResultSet rs = pStmt.executeQuery();

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {
		int sid = rs.getInt("id");

		int id = rs.getInt("user_id");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		int categoryid = rs.getInt("category_id");
		String mid = rs.getString("name");
		User user = new User(sid,id,recruitmentdate,dateandtime,categoryid,mid);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}



public List<User> sessionsearch2(int categoryid2) {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id  WHERE user_date.id = ?;";

	// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
	pStmt.setInt(1, categoryid2);

	ResultSet rs = pStmt.executeQuery();

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {
		int sid = rs.getInt("id");

		int id = rs.getInt("user_id");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		int categoryid = rs.getInt("category_id");
		String mid = rs.getString("name");
		User user = new User(sid,id,recruitmentdate,dateandtime,categoryid,mid);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}
public List<User> sessionsearch3(int categoryid2) {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id  WHERE user_date.id = ?;";

	// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
	pStmt.setInt(1, categoryid2);

	ResultSet rs = pStmt.executeQuery();

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {
		int sid = rs.getInt("id");

		int id = rs.getInt("user_id");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		int categoryid = rs.getInt("category_id");
		String mid = rs.getString("name");
		User user = new User(sid,id,recruitmentdate,dateandtime,categoryid,mid);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}
public List<User> sessionsearch3(String id2) {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM session_date inner join user_date on session_date.user_id = user_date .id  WHERE user_date.id = ?;";

	// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
	pStmt.setString(1, id2);

	ResultSet rs = pStmt.executeQuery();

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {
		int sid = rs.getInt("id");

		int id = rs.getInt("user_id");
		Date recruitmentdate = rs.getDate("recruitment_date");
		Date dateandtime = rs.getDate("date_and_time");
		int categoryid = rs.getInt("category_id");
		String mid = rs.getString("name");
		User user = new User(sid,id,recruitmentdate,dateandtime,categoryid,mid);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}
public List<User> findAll_user() {
Connection conn = null;
List<User> userList = new ArrayList<User>();

try {
	// データベースへ接続
	conn = DBManager.getConnection();

	// SELECT文を準備
	// TODO: 未実装：管理者以外を取得するようSQLを変更する
	String sql = "SELECT * FROM user_date ;";

	// SELECTを実行し、結果表を取得
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(sql);

	// 結果表に格納されたレコードの内容を
	// Userインスタンスに設定し、ArrayListインスタンスに追加
	while (rs.next()) {


		int  id = rs.getInt("id");
		String yname = rs.getString("name");
		Boolean voicechatflg = rs.getBoolean("voice_chat_flg");



		User user = new User(id,yname,voicechatflg);
		userList.add(user);
	}
} catch (SQLException e) {
	e.printStackTrace();
	return null;
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
return userList;
}


public List<User> userSearch(String name,String voicechat) {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		// TODO: 未実装：管理者以外を取得するようSQLを変更する
		String sql = "SELECT * FROM user_date  WHERE";


		if (!name.equals("")) {
			sql += " name LIKE '%" + name + "%'";
		}
		if (!voicechat.equals("")) {
			sql += " AND voice_chat_flg = '" + voicechat + "'";
		}


		// SELECTを実行し、結果表を取得
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		// 結果表に格納されたレコードの内容を
		// Userインスタンスに設定し、ArrayListインスタンスに追加
		while (rs.next()) {

			int  yid = rs.getInt("id");
			String yname = rs.getString("name");
			Boolean voicechatflg = rs.getBoolean("voice_chat_flg");



			User user = new User(yid,yname,voicechatflg);
			userList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return userList;
}

public void Rewrite(String name, String password, Boolean voice, int Id) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "UPDATE user_date  SET name = ?,password = ?,voice_chat_flg = ?  WHERE id = ?;";

		//暗号化



		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, name);
		pStmt.setString(2, password);
		pStmt.setBoolean(3, voice);
		pStmt.setInt(4, Id);




		int result = pStmt.executeUpdate();

		System.out.println(result);
		pStmt.close();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}






}








// 暗号化メソッド
