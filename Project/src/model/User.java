package model;

import java.io.Serializable;
import java.sql.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {


	private int id;
	private String loginId;
	private String name;
    private String password ;
	private Boolean voicechat;
	private Date recruitment_date;
	private Date date_and_time;
	private int category_id;
	private int userid;
	private String text;






	public User(int id, String name,String password_date) {
		this.id = id;
		this.password = password_date;
		this.name = name;
	}
	public User(int id, String name,String password_date,Boolean voice) {
		this.id = id;
		this.password = password_date;
		this.name = name;
		this.voicechat = voice;

	}

	public User(int sid,int id,Date recruitment_date,Date date_and_time,int category_id,String name) {
        this.id = sid;
		this.userid = id;
		this.recruitment_date = recruitment_date;
		this.date_and_time = date_and_time;
		this.category_id = category_id;
		this.name = name;


	}
	public User(int id,Date recruitment_date,Date date_and_time,int category_id,String name) {

		this.userid = id;
		this.recruitment_date = recruitment_date;
		this.date_and_time = date_and_time;
		this.category_id = category_id;
		this.name = name;


	}
	public User(int id,String yname,Boolean voicechatflg) {

		this.id = id;
		this.name = yname;
		this.voicechat = voicechatflg;

	}
	public User(int id,String password,Date recruitmentdate,Date dateandtime,Boolean voicechat,int categoryid,int mid,String text) {


		this.id = id;
		this.password = password;
		this.recruitment_date = recruitmentdate;
		this.date_and_time = dateandtime;
		this.voicechat =voicechat;
		this.category_id = categoryid;
		this.userid = mid;
		this.text = text;



	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getVoicechat() {
		return voicechat;
	}

	public void setVoicechat(Boolean voicechat) {
		this.voicechat = voicechat;
	}
	public Date getRecruitment_date() {
		return recruitment_date;
	}
	public void setRecruitment_date(Date recruitment_date) {
		this.recruitment_date = recruitment_date;
	}
	public Date getDate_and_time() {
		return date_and_time;
	}
	public void setDate_and_time(Date date_and_time) {
		this.date_and_time = date_and_time;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}





}

