package MYWebSite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class sessioncreate
 */
@WebServlet("/sessioncreate")
public class sessioncreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public sessioncreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sessioncreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String userid = request.getParameter("userid");
		String password = request.getParameter("password");
		String voice = request.getParameter("onoroff");
		String rimitdate = request.getParameter("rimitday");
		String opendate = request.getParameter("openday");
		String ct = request.getParameter("ct");
		String detailedtext = request.getParameter("comment");


		boolean voicechat;



		if(voice.equals("0")) {

			voicechat = false;


		}else {

			voicechat = true;

		}




		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userdao = new UserDao();
		userdao.sessionsignup(password,detailedtext,rimitdate,opendate,voicechat,ct,userid);

		response.sendRedirect("userdate");
	}
}



