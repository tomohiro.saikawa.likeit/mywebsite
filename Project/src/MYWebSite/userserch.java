package MYWebSite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class userserch
 */
@WebServlet("/userserch")
public class userserch extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userserch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		UserDao userDao = new UserDao();
		request.setCharacterEncoding("UTF-8");
		List<User> userList = userDao.findAll_user();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);







		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userserch.jsp");
		dispatcher.forward(request, response);




	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		UserDao userDao2 = new UserDao();
		request.setCharacterEncoding("UTF-8");


		String name = request.getParameter("name");
		String voice = request.getParameter("onoroff");






		List<User> userList=userDao2.userSearch(name,voice);
		request.setAttribute("userList", userList);




		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userserch.jsp");
		dispatcher.forward(request, response);






	}

}
