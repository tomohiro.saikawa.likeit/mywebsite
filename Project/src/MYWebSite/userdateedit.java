package MYWebSite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class userdateedit
 */
@WebServlet("/userdateedit")
public class userdateedit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userdateedit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdateedit.jsp");
		dispatcher.forward(request, response);



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");

		int id = user.getId();

		// リクエストパラメータの入力項目を取得
		String password = request.getParameter("name");
		String name = request.getParameter("password");
		String voice = request.getParameter("onoroff");


		boolean voicechat;



		if(voice.equals("0")) {

			voicechat = false;


		}else {

			voicechat = true;

		}




		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userdao = new UserDao();
		userdao.Rewrite(name, password, voicechat,id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdate.jsp");
		dispatcher.forward(request, response);




	}

}
